# Simplified Mapy.cz API

This library works with standard Mapy.cz API, but it's nicer and more easy-to-use. However, the only minor API capabilities are not implemented and it can be used to advanced mapy.cz scenarios.

The map is loaded asynchonously, so it won't break your page speed indexes. It's built with page speed in mind.

The library can be used on any website. You can find example WordPress plugin with implementation here:
https://gitlab.com/wpify/test-mapy-cz 

## Installation

```bash
npm install wpify-mapy-cz --save
```

or

```bash
yarn add wpify-mapy-cz
```

## Usage

```js
import { load, MapyCz } from 'wpify-mapy-cz';

// Setup configuration object
const config = {
  element: document.getElementById('some-map-div-id'), // this is only required parameter
  center: { latitude: 50.11968806014661, longitude: 14.42896842864991 },
  zoom: 13,
};

// 1: Load the map with callback function

load(config, (mapycz) => {
  // some map manipulation after load on callback
});

// 2: or with Premise

const mapycz = new MapyCz(config);
mapycz.resolver.then(mapycz => {
  // some map manipulation after load in Promise
});

// 3: or with async
const mapycz = new MapyCz(config);
await mapycz.resolver;
// some map manipulation

// 4: or in case you need to configure the map on the first render without further manipulations

load(config);
```

## Configuration object

* `element`: **domelement, required**; Element where you want to render the map
* `lang`: **string**; Language in the map. Accepts any of `cs`, `en`, `de`, `sk`, `pl`
* `mapType`: **string**; Type of the map; *default* `DEF_BASE`
* `api`: **string**; Loads `full` or `simple` map; *default* `full`
* `center`: **object**; Center of the map in the `{ longitude: 0, latitude: 0 }` format; *default* `{ longitude: 0, latitude: 0 }`
* `zoom`: **number**; Zoom level; *default 13*
* `default_controls`: **boolean**; If true, adds default map controls
* `sync_control`: **boolean**; If true, synchronizes the map with element size changes
* `markers`: **array**; Adds multiple markers (see the marker format bellow)
* `marker`: **object**; Adds a marker (see the marker format bellow)
* `auto_center_zoom`: **bool**; Set the center and zoom to fit the markers
* `clusterers`: **string|array|boolean**; Sets the clusterer to the marker layer(s)
* `poi`: **boolean**; Show points of interest
* `map_type_switch`: **array|boolean**; Shows the map type switcher. Accepts boolean or array of map types
* `image_overlay`: **array|object**; Sets the image overlay. Accepts array of overlays or overlay definition
* `gpx`: **array|object**; Sets the GPX. Accepts array of GPXs or single GPX definition

```js
const config = {
  element: document.getElementById('some-map-div-id'),
  center: { latitude: 50.11968806014661, longitude: 14.42896842864991 },
  zoom: 13,
  ...
};
```

## Map manupulation methods

### `update` Updates the map based on configuration object

```js
mapycz.update({
  // configuration object
});
```

### `getMap` Returns the original map object `new SMap`

```js
const smap = mapycz.getMap();
```

### `setZoom` Sets the map zoom level

```js
mapycz.setZoom(13);
```

### `addDefaultControls` Adds mapy.cz default controls

```js
mapycz.addDefaultControls();
```

### `addControlSync` Sync map with viewport change

```js
mapycz.addControlSync();
```

### `addMarker` Adds marker to the map

```js
const options = {
  latitude: 50.07520039245642,
  longitude: 14.35905933288575,
  id: 'marker-1',
  layer: 'markers',
  title: 'Marker 1 title',
  pin: 'https://placekitten.com/20/30',
  pointer: true,
  card: { header: 'Marker 1', body: 'This is Marker 1', footer: 'This is Marker 1 footer' },
  click: (event, options, marker) => console.log(event, options, marker),
}

mapycz.addMarker(options);
```

### `addMarkers` Adds multiple markers to the map

```js
const markers = [{
  latitude: 50.07520039245642,
  longitude: 14.35905933288575,
  id: 'marker-1',
  layer: 'markers', // Layer where place the marker, default 'markers' 
  title: 'Marker 1 title',
  pin: 'https://placekitten.com/20/30',
  pointer: true,
  card: { header: 'Marker 1', body: 'This is Marker 1', footer: 'This is Marker 1 footer' },
  click: (event, options, marker) => console.log(event, options, marker),
}, {
  latitude: 50.084398631374334,
  longitude: 14.497203825988777,
  id: 'b1',
  layer: 'b',
  pin: 'https://placekitten.com/20/20'
}];

mapycz.addMarkers(markers);
```

### `removeMarkers` Removes all markers from the map

```js
mapycz.removeMarkers();
```

### `autoCenterZoom` Sets center and zoom to fit all the markers

```js
mapycz.autoCenterZoom();
```

### `setCenter` Sets the center of the map

```js
mapycz.setCenter({
  latitude: 50.07520039245642,
  longitude: 14.35905933288575
});
```

### `addClusterer` Sets the clusterer to the markers layer

```js
mapycz.addClusterer();
// or
mapycz.addClusterer('some-marker-layer-name');
```

### `addClusterers` Sets the clusterers to the all marker layers

```js
mapycz.addClusterers();
```

### `addPoi` Displays points of interests on the map

```js
mapycz.addPoi();
```

### `removePoi` Remove points of interests from the map

```js
mapycz.removePoi();
```

### `addMapTypeSwitch` Shows the map type switcher

```js
mapycz.addMapTypeSwitch(['DEF_BASE', 'DEF_TURIST', 'DEF_OPHOTO']);
// or
mapycz.addMapTypeSwitch(); // all available map types
```

### `removeMapTypeSwitch` Remove map types switcher

```js
mapycz.removeMapTypeSwitch();
```

### `addImageOverlay` Adds an image overlay to the map

```js
mapycz.addImageOverlay({
  id: 'some-id',
  image: 'https://placekitten.com/300/300',
  topLeft: { latitude: 50.07520039245642, longitude: 14.35905933288575 },
  bottomRight: { latitude: 50.07564106690275, longitude: 14.458279608764656 },
  opacity: 0.5,
});
```

### `removeImageOverlay` Removes image overlays from the map

```js
mapycz.removeImageOverlay();
```

### `addGpx` Adds a GPX into map

The data OR source must be defined to render the GPX data

```js
mapycz.addGpx({
  id: 'some-gpx-id', // optional GPX layer id
  data: '<?xml version="1.0" encoding="UTF-8"?><gpx ...>...</gpx>', // XML document with GPX
  source: 'https://www.wpify-mapy-cz.test/test.gpx', // URL to the XML document with GPX
  fit: true, // fit the map to the gpx route
  colors: ['red'], // color of the gpx route
  maxPoints: 500, // max points to render
});
```

### `removeGpx` Removes GPX layers from the map

```js
mapycz.removeGpx();
```

### `hideLayer` Hides the layer

```js
mapycz.hideLayer('markers');
```

### `hideAllLayers` Hides all layers of given type

```js
mapycz.hideAllLayers('markers');
```

### `showLayer` Shows the layer

```js
mapycz.showLayer('markers');
```

### `showAllLayers` Hides all layers of given type

```js
mapycz.showAllLayers('markers');
```

### `isLayerVisible` Returns true if the layer with specific id is currently visible

```js
mapycz.isLayerVisible('markers');
```

### `findByAddress` Finds the location by address

```js
mapycz.findByAddress('Prague')
  .then(console.log)
```

### `findByCoords` Finds the location address by coords

```js
mapycz.findByCoords({ latitude: 50.11968806014661, longitude: 14.42896842864991 })
  .then(console.log);
```

### `addSuggest` Registers a suggest box to find the address

```js
mapycz.addSuggest({
  input: document.getElementById('some-input-id'),
  lang: 'cs',
}).then((place) => console.log('selected place', place));
```

## Enabled map types

* `DEF_BASE`
* `DEF_TURIST`
* `DEF_OPHOTO` 
* `DEF_HISTORIC`
* `DEF_OPHOTO0203`
* `DEF_OPHOTO0406`
* `DEF_SMART_BASE`
* `DEF_SMART_OPHOTO`
* `DEF_SMART_TURIST`
* `DEF_TURIST_WINTER`
* `DEF_SMART_WINTER`
* `DEF_GEOGRAPHY`
* `DEF_OPHOTO1012`
* `DEF_OPHOTO1415`
* `DEF_OPHOTO1618`
* `DEF_BASE_NEW`
* `DEF_TURIST_NEW`