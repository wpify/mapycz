/* globals SMap, JAK, Loader */

export class MapyCz {
  /**
   * Definitions
   */
  id = null;
  controls = {};
  layers = {};
  markers = {};
  markersDefinition = {};
  clusterers = {};
  images = {};
  mapTypes = {
    DEF_BASE: 'DEF_BASE',
    DEF_TURIST: 'DEF_TURIST',
    DEF_OPHOTO: 'DEF_OPHOTO',
    DEF_HISTORIC: 'DEF_HISTORIC',
    DEF_OPHOTO0203: 'DEF_OPHOTO0203',
    DEF_OPHOTO0406: 'DEF_OPHOTO0406',
    DEF_SMART_BASE: 'DEF_SMART_BASE',
    DEF_SMART_OPHOTO: 'DEF_SMART_OPHOTO',
    DEF_SMART_TURIST: 'DEF_SMART_TURIST',
    DEF_TURIST_WINTER: 'DEF_TURIST_WINTER',
    DEF_SMART_WINTER: 'DEF_SMART_WINTER',
    DEF_GEOGRAPHY: 'DEF_GEOGRAPHY',
    DEF_OPHOTO1012: 'DEF_OPHOTO1012',
    DEF_OPHOTO1415: 'DEF_OPHOTO1415',
    DEF_OPHOTO1618: 'DEF_OPHOTO1618',
    DEF_BASE_NEW: 'DEF_BASE_NEW',
    DEF_TURIST_NEW: 'DEF_TURIST_NEW',
  };

  /**
   * Initialize map
   * @param params
   */
  constructor (params = {}) {
    this.loaderUrl = 'https://api.mapy.cz/loader.js';
    this.lang = params.lang || 'en';
    this.zoom = params.zoom || 13;
    this.mapType = params.mapType || this.mapTypes.DEF_BASE;
    this.api = ['full', 'simple'].includes(params.api) ? params.api : 'full';
    this.poi = params.poi ? Boolean(params.poi) : true;
    this.pano = params.pano ? Boolean(params.pano) : true;
    this.suggest = params.suggest ? Boolean(params.pano) : true;

    if (params.center && params.center.latitude && params.center.longitude) {
      this.center = params.center;
    } else {
      this.center = { latitude: 0, longitude: 0 };
    }

    let element;

    if (params.element) {
      element = params.element;
    } else if (params.id && document.getElementById(params.id)) {
      element = document.getElementById(params.id);
    }

    if (!document.getElementById('wpify-mapy-cz-css-reset')) {
      const style = document.createElement('style');
      style.id = 'wpify-mapy-cz-css-reset';
      style.appendChild(
        document.createTextNode([
          '.smap{min-height:170px}',
          '.smap img{max-width:unset!important;display:unset!important;}',
          '.smap,.smap *,.smap *::before,.smap *::after{word-break:normal!important;box-sizing:unset!important;}',
          '.smap svg{display:unset!important;height:unset!important;max-width:unset!important;}',
        ].join(' '))
      );
      document.head.appendChild(style);
    }

    if (element) {
      this.element = element;
      this.id = params.id;
      this.resolver = new Promise((resolve, reject) => {
        this.loadScript()
          .then(this.loadSmap)
          .then(this.createMap)
          .then(() => {
            this.update(params);
            resolve(this);
          })
          .catch(reject);
      });
    }
  }

  /**
   * CORE METHODS
   * ============
   */

  update = (params = {}) => {
    if (params.zoom) {
      this.setZoom(params.zoom);
    }

    if (params.center && this.isCoords(params.center)) {
      this.setCenter(params.center);
    }

    if (params.default_controls) {
      this.addDefaultControls();
    }

    if (params.sync_control) {
      this.addControlSync();
    } else {
      this.removeControl('sync');
    }

    if (params.markers && Array.isArray(params.markers)) {
      this.addMarkers(params.markers);
    } else {
      this.removeMarkers();
    }

    if (params.marker && this.isCoords(params.marker)) {
      this.addMarker(params.marker);
    }

    if (params.auto_center_zoom) {
      this.autoCenterZoom();
    }

    if (params.clusterers) {
      if (Array.isArray(params.clusterers)) {
        params.clusterers.forEach(this.addClusterer);
      } else if (typeof params.clusterers === 'string') {
        this.addClusterer(params.clusterers);
      } else {
        this.addClusterers();
      }
    }

    if (params.poi) {
      this.addPoi();
    } else {
      this.removePoi();
    }

    if (params.map_type_switch) {
      if (Array.isArray(params.map_type_switch)) {
        this.addMapTypeSwitch(params.map_type_switch);
      } else {
        this.addMapTypeSwitch();
      }
    } else {
      this.removeMapTypeSwitch();
    }

    if (params.image_overlay) {
      if (Array.isArray(params.image_overlay)) {
        params.image_overlay.forEach(this.addImageOverlay);
      } else {
        this.addImageOverlay(params.image_overlay);
      }
    } else {
      this.removeImageOverlay();
    }

    if (params.gpx) {
      if (Array.isArray(params.gpx)) {
        params.gpx.forEach(this.addGpx);
      } else {
        this.addGpx(params.gpx);
      }
    } else {
      this.removeGpx();
    }
  };

  loadScript = () => {
    if (window.wpifyMapyCzScript && window.wpifyMapyCzScript instanceof Promise) {
      return window.wpifyMapyCzScript;
    }

    window.wpifyMapyCzScript = new Promise((resolve, reject) => {
      if (document.querySelector('script[src="' + this.loaderUrl + '"]')) {
        resolve();
      } else {
        const script = document.createElement('script');
        const fjs = document.querySelector('script');

        script.addEventListener('load', resolve);
        script.addEventListener('error', e => reject(e.error));
        script.src = this.loaderUrl;
        fjs.parentNode.insertBefore(script, fjs);
      }
    });

    return window.wpifyMapyCzScript;
  };

  loadSmap = () => {
    if (window.wpifyMapyCzScriptLoader && window.wpifyMapyCzScriptLoader instanceof Promise) {
      return window.wpifyMapyCzScriptLoader;
    }

    window.wpifyMapyCzScriptLoader = new Promise((resolve) => {
      Loader.async = true;
      Loader.lang = this.lang;
      Loader.load(this.id, { api: this.api, poi: this.poi, pano: this.pano, suggest: this.suggest }, resolve);
    });

    return window.wpifyMapyCzScriptLoader;
  };

  createMap = () => {
    return new Promise((resolve) => {
      if (this.api === 'full') {
        this.map = new SMap(this.element, this.getCoords(this.center), this.zoom);
        this.map.addDefaultLayer(this.getMapType()).enable();
      } else if (this.api === 'simple') {
        this.map = new SMap(this.element, this.getCoords(this.center), this.zoom, this.getMapType());
      }

      this.map.getSignals().addListener(window, 'marker-click', this.onMarkerClick);

      resolve(this);
    });
  };

  getMap = () => this.map;

  /**
   * ==============
   * HELPER METHODS
   * ==============
   */


  getCoords = ({ latitude, longitude }) => {
    return SMap.Coords.fromWGS84(longitude, latitude);
  };

  getMapType = () => {
    if (SMap[this.mapType]) {
      return SMap[this.mapType];
    }

    return SMap.DEF_BASE;
  };

  generateId = ({ length = 32 } = {}) => {
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const result = [];

    for (let i = 0; i < length; i++) {
      result.push(chars.charAt(Math.floor(Math.random() * chars.length)));
    }

    return result.join('');
  };

  isCoords = (coords) => typeof coords === 'object' && coords.longitude && coords.latitude;

  transformCoords = (coords) => {
    if (typeof coords === 'object' && coords.x && coords.y) {
      const latitude = coords.y;
      const longitude = coords.x;

      return { latitude, longitude };
    }

    return coords;
  };

  /**
   * =========
   * LISTENERS
   * =========
   */

  addEventListener = (event, callback) => {
    if (event === 'map-click') {
      this.map.getSignals().addListener(window, 'map-click', (e) => {
        callback(this.transformCoords(SMap.Coords.fromEvent(e.data.event, this.map)));
      });
    }
  };

  onMarkerClick = (event) => {
    const marker = event.target;
    const id = marker.getId();

    if (this.markersDefinition[id] && typeof this.markersDefinition[id].click === 'function') {
      this.markersDefinition[id].click(event, this.markersDefinition[id], marker);
    }
  };

  /**
   * ================
   * MAP MANIPULATION
   * ================
   */

  setZoom = (zoom) => {
    if (this.map.zoomChange(zoom)) {
      this.map.setZoom(zoom);
    }
  };

  addDefaultControls = () => {
    this.map.addDefaultControls();
  };

  addControlSync = (options) => {
    this.removeControl('sync');
    this.controls['sync'] = new SMap.Control.Sync(options);
    this.map.addControl(this.controls['sync']);
  };

  removeControl = (id) => {
    if (this.controls[id]) {
      this.map.removeControl(id);
      delete this.controls[id];
    }
  };

  addMarker = (options) => {
    const id = options.id || this.generateId();
    this.addMarkers([{ ...options, id }]);

    return id;
  };

  addMarkers = (markers = [], layer = 'markers') => {
    const layers = {};

    markers.forEach(marker => {
      const layer = marker.layer || layer;

      if (!layers[layer]) {
        layers[layer] = [];
      }

      layers[layer].push(marker);
    });

    Object.keys(layers).forEach((layer) => {
      if (this.layers[layer]) {
        this.layers[layer].disable();
      } else {
        this.layers[layer] = new SMap.Layer.Marker();
        this.map.addLayer(this.layers[layer]);
      }

      layers[layer].forEach((options) => {
        const {
          latitude,
          longitude,
          id = this.generateId(),
          title,
          size = null,
          pin,
          anchor,
          card,
          pointer,
        } = options;

        this.markersDefinition[id] = options;

        const coords = this.getCoords({ latitude, longitude });
        const args = {};

        if (title) {
          args.title = title;
        }

        if (size) {
          args.size = size;
        }

        if (pin) {
          args.url = pin;
        }

        if (anchor) {
          args.anchor = anchor;
        }

        this.removeMarker(id, layer);

        this.markers[id] = new SMap.Marker(coords, id, args);

        if (card && Object(card) === card) {
          const { header, body, footer, container } = card;
          const popup = new SMap.Card();

          if (header) {
            popup.getHeader().innerHTML = header;
          }

          if (body) {
            popup.getBody().innerHTML = body;
          }

          if (footer) {
            popup.getFooter().innerHTML = footer;
          }

          if (container) {
            popup.getContainer().innerHTML = container;
          }

          this.markers[id].decorate(SMap.Marker.Feature.Card, popup);
        }

        this.layers[layer].addMarker(this.markers[id]);

        if (pointer) {
          const controlId = 'marker-pointer-' + id;

          if (this.controls[controlId]) {
            this.removeControl(controlId);
          }

          this.controls[controlId] = new SMap.Control.Pointer();
          this.controls[controlId].addListener('pointer-click', () => this.setCenter({
            latitude,
            longitude
          }), this.controls[controlId]);
          this.controls[controlId].setCoords(coords);
          this.map.addControl(this.controls[controlId]);
        }
      });

      this.layers[layer].enable();
    });
  };

  removeMarker = (id, layer = 'markers') => {
    if (this.markers[id]) {
      if (this.layers[layer]) {
        this.layers[layer].removeMarker(this.markers[id]);
      }
      delete this.markers[id];
    }
  };

  removeMarkers = () => {
    Object.keys(this.layers).forEach(layer => {
      if (this.layers[layer] && this.layers[layer] instanceof SMap.Layer.Marker) {
        this.layers[layer].removeAll();
      }
    });
  };

  autoCenterZoom = (layer = null) => {
    const markers = (layer && this.layers[layer])
      ? this.layers[layer].getMarkers()
      : Object.values(this.markers);

    const [coords, zoom] = this.map.computeCenterZoom(
      markers.map(marker => marker.getCoords())
    );

    this.map.setCenterZoom(coords, zoom);
  };

  setCenter = (coords) => {
    this.map.setCenter(this.getCoords(coords));
  };

  addClusterer = (layer = 'markers') => {
    if (this.layers[layer]) {
      const clusterer = new SMap.Marker.Clusterer(this.map);
      this.layers[layer].setClusterer(clusterer);
      this.clusterers[layer] = clusterer;
    }
  };

  addClusterers = () => {
    Object.keys(this.layers).forEach(layer => {
      if (this.layers[layer] && this.layers[layer] instanceof SMap.Layer.Marker) {
        this.addClusterer(layer);
      }
    });
  };

  addPoi = () => {
    if (!this.layers.poi) {
      this.layers.poi = new SMap.Layer.Marker(undefined, { poiTooltip: true });
      this.map.addLayer(this.layers.poi).enable();

      const dataProvider = this.map.createDefaultDataProvider();
      dataProvider.setOwner(this.map);
      dataProvider.addLayer(this.layers.poi);
      dataProvider.setMapSet([
        this.mapTypes.DEF_TURIST,
        this.mapTypes.DEF_TURIST_WINTER,
        this.mapTypes.DEF_TURIST_NEW,
        this.mapTypes.DEF_SMART_TURIST
      ].includes(this.mapType) ? SMap.MAPSET_TURIST : SMap.MAPSET_BASE);
      dataProvider.enable();
    }
  };

  removePoi = () => {
    if (this.layers.poi) {
      this.map.removeLayer(this.layers.poi);
      delete this.layers.poi;
    }
  };

  addMapTypeSwitch = (
    mapTypes,
    options = {
      width: 65,
      items: 4,
      page: 4,
    }
  ) => {
    const maps = Array.isArray(mapTypes) ? mapTypes.filter(t => Boolean(SMap[t])) : Object.keys(this.mapTypes).filter(t => Boolean(SMap[t]));

    maps.forEach(m => {
      this.map.addDefaultLayer(SMap[m]);
    });

    const layerSwitch = new SMap.Control.Layer(options);

    maps.forEach(m => {
      layerSwitch.addDefaultLayer(SMap[m]);
    });

    if (this.controls['layer-switch']) {
      this.removeControl('layer-switch');
    }

    this.controls['layer-switch'] = layerSwitch;
    this.map.addControl(layerSwitch);
  };

  removeMapTypeSwitch = () => {
    if (this.controls['layer-switch']) {
      this.removeControl('layer-switch');
    }
  };

  addImageOverlay = (params = {}) => {
    const {
      id = this.generateId(),
      image,
      top_left,
      topLeft = top_left,
      bottom_right,
      bottomRight = bottom_right,
      opacity = 1
    } = params;

    if (!this.layers['image']) {
      this.layers['image'] = new SMap.Layer.Image();
      this.map.addLayer(this.layers['image']);
      this.layers['image'].enable();
    }

    if (image && topLeft && bottomRight) {
      if (this.images[id]) {
        this.layers['image'].removeImage(this.images[id]);
      }

      this.images[id] = this.layers['image'].addImage(
        image,
        this.getCoords(topLeft),
        this.getCoords(bottomRight),
        opacity
      );

      return this.images[id];
    }
  };

  removeImageOverlay = () => {
    if (this.layers['image']) {
      Object.keys(this.images).forEach(id => {
        this.layers['image'].removeImage(this.images[id]);
        delete this.images[id];
      });
    }
  };

  addGpx = ({ id = this.generateId(), data, source, fit, colors, maxPoints, url }) => {
    const fetchXml = new Promise((resolve, reject) => {
      if (data) {
        const xml = JAK.XML.createDocument(data);
        resolve(xml);
      } else if (source) {
        fetch(source)
          .then(response => {
            if (response.ok) {
              return response.text();
            }

            throw response.error();
          })
          .then(data => {
            resolve(JAK.XML.createDocument(data));
          })
          .catch(reject);
      }
    });

    fetchXml.then((xml) => {
      const layerId = 'gpx-' + id;
      const params = {};

      if (colors) {
        params.colors = colors;
      }

      if (maxPoints) {
        params.maxPoints = maxPoints;
      }

      if (url) {
        params.url = url;
      }

      this.layers[layerId] = new SMap.Layer.GPX(xml, null, params);
      this.map.addLayer(this.layers[layerId]);
      this.layers[layerId].enable();

      if (fit) {
        this.layers[layerId].fit();
      }

      return Promise.resolve(layerId);
    });
  };

  removeGpx = () => {
    Object.keys(this.layers).forEach(id => {
      if (this.layers[id] instanceof SMap.Layer.GPX) {
        this.removeLayer(this.layers[id]);
        delete this.layers[id];
      }
    });
  };

  isLayerVisible = (id) => {
    if (this.layers[id]) {
      return this.layers[id].isActive();
    }
  };

  hideLayer = (id) => {
    if (this.layers[id]) {
      this.layers[id].disable();
    }
  };

  showLayer = (id) => {
    if (this.layers[id]) {
      this.layers[id].enable();
    }
  };

  hideAllLayers = (type = 'markers') => {
    Object.keys(this.layers).forEach(id => {
      if (type === 'markers' && this.layers[id] instanceof SMap.Layer.Marker) {
        this.layers[id].disable();
      }
    });
  };

  showAllLayers = (type = 'markers') => {
    Object.keys(this.layers).forEach(id => {
      if (type === 'markers' && this.layers[id] instanceof SMap.Layer.Marker) {
        this.layers[id].enable();
      }
    });
  };

  /**
   * =======
   * METHODS
   * =======
   */

  findByAddress = (address, boundingBox) => new Promise((resolve, reject) => {
    const evaluate = (geocoder) => {
      const results = [];
      const respres = geocoder.getResults();

      if (respres.length > 0 && respres[0].results.length > 0) {
        respres.forEach(res => {
          results.push(...res.results);
        });

        resolve(results.map(result => {
          const coords = this.transformCoords(result.coords);
          delete result.coords;

          return { ...result, ...coords };
        }));
      } else {
        reject();
      }
    };

    if (boundingBox
      && boundingBox.length === 2
      && this.isCoords(boundingBox[0])
      && this.isCoords(boundingBox[1])
    ) {
      new SMap.Geocoder(address, evaluate, {
        bbox: [
          this.getCoords(boundingBox[0]),
          this.getCoords(boundingBox[1]),
        ],
      });
    } else {
      new SMap.Geocoder(address, evaluate);
    }
  });

  findByCoords = (coords) => new Promise((resolve, reject) => {
    if (this.isCoords(coords)) {
      const evaluate = (geocoder) => {
        const result = geocoder.getResults();
        result.coords = this.transformCoords(result.coords);
        result.items = result.items.map(item => {
          item.coords = this.transformCoords(item.coords);
          return item;
        });
        resolve(result);
      };

      new SMap.Geocoder.Reverse(this.getCoords(coords), evaluate);
    } else {
      reject('No coords provided');
    }
  });

  addSuggest = ({ input, lang = 'cs,en', bounds, enableCategories }, callback) => {
    if (!input) {
      return false;
    }

    this.suggest = new SMap.Suggest(input, {
      provider: new SMap.SuggestProvider({
        updateParams: params => {
          let c = this.map.getCenter().toWGS84();
          params.lon = c[0].toFixed(5);
          params.lat = c[1].toFixed(5);
          params.zoom = this.map.getZoom();

          if (bounds) {
            params.bounds = bounds;
          }

          if (enableCategories) {
            params.enableCategories = enableCategories;
          }

          params.lang = lang;
        }
      })
    });

    this.suggest.addListener('suggest', callback);
  };
}

/**
 * @callback mapyczCallback
 * @param {MapyCz} mapycz
 */
/**
 * @param {Object} params params
 * @param {mapyczCallback} callback mapyczCallback
 */
export const load = (params, callback = () => null) => {
  const api = new MapyCz(params);
  api.resolver.then(callback);
};

export default MapyCz;
