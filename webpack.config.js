const path = require('path');

module.exports = {
  entry: './src/wpify-mapy-cz.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'wpify-mapy-cz.min.js',
    library: {
      name: {
        root: 'WpifyMapyCz',
        amd: 'wpify-mapy-cz',
        commonjs: 'wpify-mapy-cz',
      },
      type: 'umd',
    },
  },
  resolve: {
    symlinks: false,
  },
  module: {
    rules: [
      {
        test: /\.m?jsx?$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  }
}